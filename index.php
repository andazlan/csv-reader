
<?php
/**
 * Created by PhpStorm.
 * User: andazlan
 * Date: 28/07/18
 * Time: 13.41
 */

require __DIR__ . '/vendor/autoload.php';
use ParseCsv\Csv;

$file_name = "";
$error = 0;
//var_dump($_FILES);die();
if (isset($_FILES['berkas']) && $_FILES && $_FILES['berkas']['name']) {
    $file_name = 'upload/'.$_FILES['berkas']['name'];
    $imageFileType = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
    if ($imageFileType != "csv") {
        $error = 1;
    }

    if ($error == 0) {
        move_uploaded_file($_FILES['berkas']['tmp_name'], $file_name);
    }
}

$file_path = $file_name;

function fixingCsvHeader($file_path) {
    $csv = new Csv($file_path);

    $tamp_title = "";
    $post = 2;
    $heading = "";
    $index = 0;

    foreach ($csv->titles as $value) {
        //var_dump($tamp_title . " " . $value); die();
        if ($tamp_title == $value) {
            $value = $value . " " .$post;
            $post++;
        }
        $tamp_title = $value;


        $heading = $heading . $value;
        $index++;
        if ((sizeof($csv->titles) - 1) != $index) {
            $heading .= ",";
        }
    }

    $heading .= "\n";

    if (isset($file_path) && $file_path) {
        $arr = file($file_path);
        $arr[0] = $heading;
        file_put_contents($file_path, implode($arr));

        $csv = new Csv($file_path);
    }

    return $csv;
}

if ($error == 0) {
    $csv = fixingCsvHeader($file_path);
}
?>

</pre>
<style type="text/css" media="screen">
    table {
        background-color: #BBB;
    }
    th {
        background-color: #EEE;
    }
    td {
        background-color: #FFF;
    }
    .error {
        color: red;
    }
</style>
<form action="index.php" method="post" enctype="multipart/form-data">
    File <input type="file" name="berkas" id="file-to-upload">
    <input type="submit">
</form>
<?
    if ($error > 0) {
        if ($error == 1) {
            echo '<span class="error">File not supported</span>';
            return;
        }
    }
?>
<table border="0" cellspacing="1" cellpadding="3">
    <tr>
        <?
            foreach ($csv->titles as $value) {
                echo '<th>'.$value.'</th>';
            }
        ?>
    </tr>
    <?
    foreach ($csv->data as $key => $row) {
        ?>
            <tr>
                <?
                    foreach ($row as $value) {
                        echo '<td>'.$value.'</td>';
                    }
                ?>
            </tr>
        <?
    }
    ?>
</table>
